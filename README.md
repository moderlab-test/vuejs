# VueJS

## Getting started

Go to [VueJs project](https://gitlab.com/moderlab-test/vuejs) and click on `fork` to get a personal copy of this project (you can keep it private).

Clone your project on your local machine and apply any changes you would like, then push them to your remote project.

You will finally be able to create a merge request from your forked project to the original one.

## Goals

You will have to create a new component (and its test file) and ensure responsiveness for the whole project. Don't hesitate to overwrite the Card component.

That component will give the ability to display a series of news entries about the construction progress.

The entries are available in `/static/json/news.json`.
